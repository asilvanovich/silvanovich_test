## 06. Ansible cruise 

First, inventory yaml file that describes managed hosts was created:  
```
all_hosts:
  children:
    ec_htp:
      vars:
        full_name: "Educational Center of HTP"
      hosts:
        jump:
          ansible_user: jump_sa
          ansible_host: 178.124.206.48
        sa_cent:
          ansble_user: asilvanovich
          ansible_host: 192.168.254.50
        sa_ubuntu:
          ansible_user: asilvanovich
          ansible_host: 192.168.254.51
webservers:
  hosts:
    sa_cent:
    sa_ubuntu:
```

Then playbook to check OS version, mount info and RAM info was created. I used information from ansible_facts to print all the required info. `ansible_distribution` and `ansible_distribution_info` to check OS and OS version, `ansible_mounts` to check mount information and `ansible_memtotal_mb` and `ansible_e_mb` to check RAM info.  
Here is the results of running this playbook:  
```
root@ubuntu:~/devops/ansible# ansible-playbook info.yaml -i inv.yaml

PLAY [webservers] **************************************************************

TASK [Gathering Facts] *********************************************************
ok: [sa_ubuntu]
ok: [sa_cent]

TASK [check distribution] ******************************************************
ok: [sa_cent] => {
    "msg": "CentOS,  7.6"
}
ok: [sa_ubuntu] => {
    "msg": "Ubuntu,  18.04"
}

TASK [check mount point and capacity] ******************************************
ok: [sa_cent] => {
    "msg": [
        {
            "block_available": 194665,
            "block_size": 4096,
            "block_total": 259584,
            "block_used": 64919,
            "device": "/dev/sda1",
            "fstype": "xfs",
            "inode_available": 523938,
            "inode_total": 524288,
            "inode_used": 350,
            "mount": "/boot",
            "options": "rw,seclabel,relatime,attr2,inode64,noquota",
            "size_available": 797347840,
            "size_total": 1063256064,
            "uuid": "25c92c64-022e-49de-9569-635ceee44ef0"
        },
        {
            "block_available": 7181234,
            "block_size": 4096,
            "block_total": 7596417,
            "block_used": 415183,
            "device": "/dev/mapper/centos-root",
            "fstype": "xfs",
            "inode_available": 15153126,
            "inode_total": 15200256,
            "inode_used": 47130,
            "mount": "/",
            "options": "rw,seclabel,relatime,attr2,inode64,noquota",
            "size_available": 29414334464,
            "size_total": 31114924032,
            "uuid": "d6300753-a4b8-453c-b989-3ba78734855c"
        }
    ]
}
ok: [sa_ubuntu] => {
    "msg": [
        {
            "block_available": 0,
            "block_size": 131072,
            "block_total": 710,
            "block_used": 710,
            "device": "/dev/loop2",
            "fstype": "squashfs",
            "inode_available": 0,
            "inode_total": 12823,
            "inode_used": 12823,
            "mount": "/snap/core/7396",
            "options": "ro,nodev,relatime",
            "size_available": 0,
            "size_total": 93061120,
            "uuid": "N/A"
        },
        {
            "block_available": 5573847,
            "block_size": 4096,
            "block_total": 8223428,
            "block_used": 2649581,
            "device": "/dev/sda2",
            "fstype": "ext4",
            "inode_available": 1937657,
            "inode_total": 2097152,
            "inode_used": 159495,
            "mount": "/",
            "options": "rw,relatime,data=ordered",
            "size_available": 22830477312,
            "size_total": 33683161088,
            "uuid": "85ee5480-175f-11e9-b21c-c21c795eb467"
        },
        {
            "block_available": 0,
            "block_size": 131072,
            "block_total": 708,
            "block_used": 708,
            "device": "/dev/loop0",
            "fstype": "squashfs",
            "inode_available": 0,
            "inode_total": 12823,
            "inode_used": 12823,
            "mount": "/snap/core/7270",
            "options": "ro,nodev,relatime",
            "size_available": 0,
            "size_total": 92798976,
            "uuid": "N/A"
        }
    ]
}

TASK [check RAM] ***************************************************************
ok: [sa_cent] => {
    "msg": "Total amount of RAM: 990, Free RAM:  242"
}
ok: [sa_ubuntu] => {
    "msg": "Total amount of RAM: 984, Free RAM:  128"
}

PLAY RECAP *********************************************************************
sa_cent                    : ok=4    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
sa_ubuntu                  : ok=4    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```  
Then I created a playbook to add a user, enable login by SSH ket to the created account and set NOPASSWD sudo rights for this user. I used `user` module to create a new user, `authorized_key` module to transfer my public key to the created user's directory and `blockinfile` module to create a file, that will grant NOPASSWD rights to the created user, in /etc/sudoers.d folder.  
```
root@ubuntu:~/devops/ansible# ansible-playbook useradd.yaml -e "user_to_add=silva" -i inv.yaml

PLAY [webservers] *******************************************************************

TASK [Gathering Facts] **************************************************************
ok: [sa_ubuntu]
ok: [sa_cent]

TASK [Create user] ******************************************************************
ok: [sa_ubuntu]
ok: [sa_cent]

TASK [key authorization] ************************************************************
changed: [sa_cent]
changed: [sa_ubuntu]

TASK [give the created user NOPASSWD rights] ****************************************
changed: [sa_ubuntu]
changed: [sa_cent]

PLAY RECAP **************************************************************************
sa_cent                    : ok=4    changed=2    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
sa_ubuntu                  : ok=4    changed=2    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

root@ubuntu:~/devops/ansible# ssh silva@192.168.254.50
[silva@sa-cent ~]$ sudo cat /etc/sudoers.d/silva
# BEGIN ANSIBLE MANAGED BLOCK
silva ALL=(ALL)  NOPASSWD: ALL
# END ANSIBLE MANAGED BLOCK
```
